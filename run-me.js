var prompt = require('prompt');
var secretSanta = require('./secret-santa');


prompt.start();

var properties = [
  { name: 'username' },
  { name: 'password', hidden: true }
];

prompt.get(properties, function (err, result) {
  if (err) { return onErr(err); }
  secretSanta.run(result.username, result.password);
});

function onErr(err) {
  console.log(err);
  return 1;
}
