var _ = require('lodash');

var secretSanta = function (username, password) {
  var names = [
      { name: 'Person 1',   buy_for: undefined , email: 'asdf@asdf.com' },
      { name: 'Person 2',   buy_for: undefined , email: 'asdf@asdf.com' }
    ]
  ;

  _.each(names, function(item, idx) {
    var random,
        count = 0;

    while (!item.buy_for) {
      random = Math.floor(Math.random()*names.length);
      if (random != idx && !_.where(names, {buy_for: names[random]}).length) {
        if(!(names[random].buy_for && _.where(names[random], {buy_for: names[idx]}).length)) {
          item.buy_for = names[random];
        }
      }
      if (count++ > 100) {
        //console.log('had to break');
        break;
      }
    }
  });

  _.each(names, function(person, idx) {
    //console.log(person.name + " buying for " + person.buy_for.name);
    var nodemailer = require("nodemailer");

    // create reusable transport method (opens pool of SMTP connections)
    var smtpTransport = nodemailer.createTransport("SMTP",{
        service: "Gmail",
        auth: {
            user: username,
            pass: password
        }
    });

    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: "Secret Santa ✔ <" + username + ">", // sender address
        to: person.name + " <" + person.email + ">", // list of receivers
        subject: "You are the secret santa for...", // Subject line
        text: "Hi " +
          person.name +
          ",\nYou have been selected to be the secret santa for " +
          person.buy_for.name, // plaintext body
        html: "" +
          "<div>" +
            "<h2>Hi " + person.name + "</h2>" +
          "</div>" +
          "<p>" +
            "You have been selected to be the secret santa for " +
            "<b>" + person.buy_for.name + "</b>" +
          "</p>" +
          "<p>" +
            "Remember not to tell anybody who you're Secret Santa to and the limit is $100" +
          "</p>"
    }

    // send mail with defined transport object
    smtpTransport.sendMail(mailOptions, function(error, response){
        if(error){
            console.log(error);
        }else{
            console.log("Message sent: " + response.message);
        }

        // if you don't want to use this transport object anymore, uncomment following line
        smtpTransport.close(); // shut down the connection pool, no more messages
    });
  })
}

exports.run = secretSanta;
